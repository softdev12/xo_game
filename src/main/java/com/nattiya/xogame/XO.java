/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nattiya.xogame;

import java.util.*;

/**
 *
 * @author DELL
 */
public class XO {

    static String xo[][] = {{"-", "-", "-"},
    {"-", "-", "-"},
    {"-", "-", "-"}};

    static String turn = "X";
    static int r, c;
    static int round = 0;

    public static void BoardXO() {
        System.out.println(" " + " 1 " + "2" + " 3 ");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < 3; j++) {
                System.out.print(" ");
                System.out.print(xo[i][j]);
            }
            System.out.println();
        }
    }

    public static void inputRowAndCol() {
        Scanner kb = new Scanner(System.in);

        for (;;) {
            System.out.println(turn + " turn");
            System.out.println("Please input Row Col:");
            String a = kb.next();
            String b = kb.next();
            r = Integer.parseInt(a);
            c = Integer.parseInt(b);
            r = r - 1;
            c = c - 1;
            round++;
            xo[r][c] = turn;
            break;

        }
    }

    public static void main(String[] args) {
        System.out.println("Welcome to XO Game");
        for (;;) {
            BoardXO();
            inputRowAndCol();
        }

    }
}
